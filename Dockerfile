FROM node:alpine

# windows path
# WORKDIR /mnt/c/Users/naufa/OneDrive - Bina Nusantara/magang/project tugas

WORKDIR /app

COPY ./package.json ./

RUN npm config set registry http://registry.npmjs.org/

RUN npm install --verbose

COPY ./ ./

EXPOSE 4001

CMD [ "npm", "start" ]